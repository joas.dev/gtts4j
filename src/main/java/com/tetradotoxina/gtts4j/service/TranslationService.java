package com.tetradotoxina.gtts4j.service;

import org.jsoup.Connection.Response;

import com.tetradotoxina.gtts4j.exception.GTTS4JException;
import com.tetradotoxina.gtts4j.model.AttributeResponse;
import com.tetradotoxina.gtts4j.model.BatchExecuteRequest;
import com.tetradotoxina.gtts4j.model.TranslationRequest;
import com.tetradotoxina.gtts4j.model.ConfigTranslation;

public interface TranslationService {
	
	String textToSpeech(TranslationRequest translationRequest) throws GTTS4JException;

	AttributeResponse findAttributes(ConfigTranslation configTranslation) throws GTTS4JException;

	String translate(TranslationRequest translationRequest) throws GTTS4JException;
	
	Response postBatchExecute(BatchExecuteRequest batchExecuteRequest) throws GTTS4JException;
	
}
