package com.tetradotoxina.gtts4j.util;

public class GTTS4JUtil {

	private static final String[][] HEADER_SPECIAL_CHARACTERS = { { "\r\n|\n|\r", "%0A" } };
	private static final String[][] BODY_SPECIAL_CHARACTERS = { { "\"", "\\\\\\\\\\\\\"" } };

	private GTTS4JUtil() {
	}

	private static String speacialCharacters(String text, String[][] speacialCharacters) {

		for (int i = 0; i < speacialCharacters.length; i++) {
			text = text.replaceAll(speacialCharacters[i][0], speacialCharacters[i][1]);
		}

		return text;
	}

	public static String fReqTextFormat(String text) {

		return speacialCharacters(text, BODY_SPECIAL_CHARACTERS);

	}

	public static String urlHeaderFormat(String text) {
		return speacialCharacters(text, HEADER_SPECIAL_CHARACTERS);

	}

}
