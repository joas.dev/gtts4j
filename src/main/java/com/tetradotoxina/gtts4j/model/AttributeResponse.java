package com.tetradotoxina.gtts4j.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class AttributeResponse {

	private String cookie;
	private String atParam;
	private String fSid;
	private String bl;
	//private String text;
	//private String lang;
	//private String langTraslate;
	
	@Builder.Default
	private String reqid = "";
	
}
