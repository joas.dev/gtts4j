package com.tetradotoxina.gtts4j;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.tetradotoxina.gtts4j.configuration.GTTS4JLang;
import com.tetradotoxina.gtts4j.exception.GTTS4JException;
import com.tetradotoxina.gtts4j.impl.GTTS4JImpl;

import lombok.extern.java.Log;


@Log
public class GTTS4JTest {
	
	private GTTS4J gtts4j;
		
	@BeforeEach
	private void init() {
		gtts4j = new GTTS4JImpl();
	}
	
	
	@Test
	void shouldSaveAFileInTheSpecifiedPath() throws GTTS4JException {
		String text = "enter the text you want to convert to speech";
		String lang="en";	
		boolean slow = false;
		
		String filePath = System.getProperty("user.dir")+File.separator+"demo.mp3";
		
		byte[] data = gtts4j.textToSpeech(text, lang, slow);
		gtts4j.saveFile(filePath, data, true);
			
		assertNotNull(data);
	}

	@Test
	void shouldTranslateTheText() throws GTTS4JException {
		String text = "enter the text you want to translate";
		String lang="en";
		String langTranslate="es";
				
		String traslate = gtts4j.translate(text, lang, langTranslate);
		log.info("Translation: "+traslate);	
		assertNotNull(traslate);
	}
	
	@Test
	void shouldSaveTheAudioFilesOfAllLanguages() throws GTTS4JException {
		
		String text = "hello world";
		String lang="en";
		String langTranslate="";
		
		File path = new File(System.getProperty("user.dir")+File.separator+"langs");
		
		if(!path.exists()) {
			path.mkdirs();
		}
		
		for(GTTS4JLang langEnum :GTTS4JLang.values()) {
						
			if(langEnum.getCode().equals(GTTS4JLang.EN.getCode())) {
				continue;
			}
			
			langTranslate = langEnum.getCode();
			String traslate = gtts4j.translate(text, lang, langTranslate);
			
			log.info("lang: "+langTranslate+" "+langEnum.getLang()+" - translation: "+traslate);
						
			String filePath = path.getPath()+File.separator+langEnum.getCode()+"_demo.mp3";
			
			byte[] data = gtts4j.textToSpeech(traslate, langTranslate);
			gtts4j.saveFile(filePath, data, true);
		
		}
		
		assertTrue(true);
	} 
	
	
	@Test
	void shouldWorkwithspecialCharacters() throws GTTS4JException {
		
		String text = "\"hello world\"";
		String lang="en";	
		boolean slow = false;
		
		String filePath = System.getProperty("user.dir")+File.separator+"demo2.mp3";
		
		byte[] data = gtts4j.textToSpeech(text, lang, slow);
		gtts4j.saveFile(filePath, data, true);
			
		assertNotNull(data);
	}
	
	
}
